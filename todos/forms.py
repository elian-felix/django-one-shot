from django.forms import BaseFormSet, DateInput, Form, ModelForm, formset_factory, CharField, IntegerField, DateField, BooleanField, HiddenInput
from todos.models import TodoItem, TodoList


class TodoForm(ModelForm):
    class Meta:
        model = TodoList
        fields = ["name"]


class TodoItemForm(ModelForm):
    class Meta:
        model = TodoItem
        fields = ["task", "due_date", "is_completed", "list"]

class InlineTodoItemForm(Form):
    id = IntegerField(label='id', widget=HiddenInput())
    task = CharField(label='task', max_length=100)
    due_date = DateField(label='due_date', widget=DateInput(attrs={'type': 'date'}))
    is_completed = BooleanField(label='is_completed', initial=False)
   

class BaseInlineTodoItemFormSet(BaseFormSet):
    pass   

TodoItemFormSet = formset_factory(InlineTodoItemForm, can_delete=True, extra=1)