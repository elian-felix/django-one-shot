from django.shortcuts import redirect, render, get_object_or_404
from todos.forms import TodoForm, TodoItemForm, TodoItemFormSet
from todos.models import TodoList, TodoItem


# Create your views here.
def todo_list_list(request):
    todos = TodoList.objects.all()

    context = {"todos": todos}

    return render(request, "todos/index.html", context)


def todo_list_detail(request, id):
    todo = get_object_or_404(TodoList, id=id)

    context = {"todo": todo}

    return render(request, "todos/detail.html", context)


def todo_list_create(request):
    if request.method == "POST":
        todo_form = TodoForm(request.POST)
        if todo_form.is_valid():
            new_todo = todo_form.save()
            print(new_todo.id)
            return redirect("todo_list_detail", id=new_todo.id)

    else:
        todo_form = TodoForm()

    context = {"todo_form": todo_form}

    return render(request, "todos/create.html", context)


def todo_list_update(request, id):
    todo = TodoList.objects.get(id=id)
    items = todo.items.all()
    if request.method == "POST":
        form_data = request.POST
        
        if form_data['name'] != todo.name:
            todo.name = form_data['name']
        for i in range(int(form_data['form-TOTAL_FORMS'])):
            curr_item = {}
            curr_item['id'] = int(form_data[f'form-{i}-id']) if form_data[f'form-{i}-id'] else -1
            curr_item['task'] = form_data[f'form-{i}-task']
            curr_item['due_date'] = form_data[f'form-{i}-due_date'] if form_data[f'form-{i}-due_date'] != '' else None
            curr_item['is_completed'] = True if form_data.get(f'form-{i}-is_completed', None) else False
            delete = True if form_data.get(f'form-{i}-DELETE', None) else False

            try:
                orig = todo.items.get(id=curr_item['id'])
                if delete:
                    orig.delete()
                else:
                    for key, value in curr_item.items():
                        setattr(orig, key, value)
                    orig.save()
            except:
                if curr_item["task"].strip() != '':
                    todo.items.create(task=curr_item['task'], due_date=curr_item["due_date"], is_completed=curr_item["is_completed"])
        
        todo.save()
        return redirect(todo_list_detail, id)
    else:
        todo_form = TodoForm(instance=todo)
        initial = [item.to_dict() for item in items]
        item_form_list = TodoItemFormSet(initial=initial)

        # item_form_list = [InlineTodoItemForm(instance=item) for item in items]
        # item_form_list.append(InlineTodoItemForm(use_required_attribute=False))

    context = {"todo_form": todo_form, 'item_form_list': item_form_list}

    return render(request, "todos/update.html", context)


def todo_list_delete(request, id):
    todo = TodoList.objects.get(id=id)
    if request.method == "POST":
        todo.delete()
        return redirect("todo_list_list")

    return render(request, "todos/delete.html")


def todo_item_create(request):
    if request.method == "POST":
        item_form = TodoItemForm(request.POST)
        if item_form.is_valid():
            new_item = item_form.save()
            print(new_item.id)
            return redirect("todo_list_detail", id=new_item.list.id)

    else:
        item_form = TodoItemForm()

    context = {"item_form": item_form}

    return render(request, "todos/create_item.html", context)


def todo_item_update(request, id):
    item = TodoItem.objects.get(id=id)
    if request.method == "POST":
        item_form = TodoItemForm(request.POST, instance=item)
        if item_form.is_valid():
            new_item = item_form.save()
            print(new_item.id)
            return redirect("todo_list_detail", id=new_item.list.id)

    else:
        item_form = TodoItemForm(instance=item)

    context = {"item_form": item_form}

    return render(request, "todos/update_item.html", context)
